import React from 'react';
import { Typography } from 'antd';

const { Title } = Typography;

const home: React.FC = () => (
  <>
    <Title>Home Page</Title>
    <Title level={4}><a href="/Login">Login</a></Title>
    <Title level={4}><a href="/Register">Register</a></Title>
  </>
);

export default home;