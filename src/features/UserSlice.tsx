import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface UserState {
  email: string;
  password: string;
  confirm: string;
  nickname: string;
  phone: string;
  website: string;
  intro: string;
  gender: string;
  agreement: boolean;
}

const initialState: UserState = {
  email: '',
  password: '',
  confirm: '',
  nickname: '',
  phone: '',
  website: '',
  intro: '',
  gender: '',
  agreement: false,
};

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setEmail: (state, action: PayloadAction<string>) => {
      state.email = action.payload;
    },
    setPassword: (state, action: PayloadAction<string>) => {
      state.password = action.payload;
    },
    setConfirm: (state, action: PayloadAction<string>) => {
      state.confirm = action.payload;
    },
    setNickname: (state, action: PayloadAction<string>) => {
      state.nickname = action.payload;
    },
    setPhone: (state, action: PayloadAction<string>) => {
      state.phone = action.payload;
    },
    setWebsite: (state, action: PayloadAction<string>) => {
      state.website = action.payload;
    },
    setIntro: (state, action: PayloadAction<string>) => {
      state.intro = action.payload;
    },
    setGender: (state, action: PayloadAction<string>) => {
      state.gender = action.payload;
    },
    setAgreement: (state, action: PayloadAction<boolean>) => {
      state.agreement = action.payload;
    },
    resetForm: (state) => {
      state.email = '';
      state.password = '';
      state.confirm = '';
      state.nickname = '';
      state.phone = '';
      state.website = '';
      state.intro = '';
      state.gender = '';
      state.agreement = false;
    },
  },
});

export const {
  setEmail,
  setPassword,
  setConfirm,
  setNickname,
  setPhone,
  setWebsite,
  setIntro,
  setGender,
  setAgreement,
  resetForm,
} = userSlice.actions;
export default userSlice.reducer;
