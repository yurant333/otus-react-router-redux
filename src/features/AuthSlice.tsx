import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface AuthState {
  username: string;
  password: string;
  rememberMe: boolean;
}

const initialState: AuthState = {
  username: '',
  password: '',
  rememberMe: false,
};

const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setUsername: (state, action: PayloadAction<string>) => {
      state.username = action.payload;
    },
    setPassword: (state, action: PayloadAction<string>) => {
      state.password = action.payload;
    },
    setRememberMe: (state, action: PayloadAction<boolean>) => {
      state.rememberMe = action.payload;
    },
    resetForm: (state) => {
      state.username = '';
      state.password = '';
      state.rememberMe = false;
    },
  },
});

export const { setUsername, setPassword, setRememberMe, resetForm } =
  authSlice.actions;
export default authSlice.reducer;
